const path = require('path')

module.exports = {
  mode: 'development',
  target: 'web',
  entry: {
    dev: path.join(__dirname, 'static', 'dev.js')
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].bundle.js'
  },
  devServer: {
    contentBase: path.join(__dirname, 'static'),
    compress: true,
    hot: true,
    liveReload: true,
    port: 9000
  }
}
