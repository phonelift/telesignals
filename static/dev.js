import teleSignalsInit from '../src/index'

const AudioCtx = new window.AudioContext()

const teleSignals = teleSignalsInit(AudioCtx)

window.addEventListener('keydown', function (event) {
  teleSignals.playDTMF(event.key)
})

document.querySelectorAll('#keypad button')
  .forEach(button => {
    button.addEventListener('click', function (event) {
      teleSignals.playDTMF(event.target.innerHTML)
    })
  })

document.querySelector('#busy')
  .addEventListener('click', function () {
    teleSignals.play('busy')
  })

document.querySelector('#ringing_tone')
  .addEventListener('click', function () {
    teleSignals.play('ringing_tone')
  })

document.querySelector('#recorder_tone')
  .addEventListener('click', function () {
    teleSignals.play('recorder')
  })

document.querySelector('#disconnect_tone')
  .addEventListener('click', function () {
    teleSignals.play('disconnect')
  })

document.querySelector('#stop_ringing_tone')
  .addEventListener('click', function () {
    teleSignals.stop()
  })

// Burger management from Bulma!
document.addEventListener('DOMContentLoaded', () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0)

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach(el => {
      el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target
        const $target = document.getElementById(target)

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active')
        $target.classList.toggle('is-active')
      })
    })
  }
})
