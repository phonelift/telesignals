const freqs = Object.freeze({
  BASE_425: 425.0,
  BASE_440: 440.0,
  // Busy
  B_480: 480.0,
  B_620: 620.0,
  // Low
  L_697: 697.0,
  L_770: 770.0,
  L_852: 852.0,
  L_941: 941.0,
  // High
  H_1209: 1209.0,
  H_1336: 1336.0,
  H_1477: 1477.0,
  H_1633: 1633.0
})

// @see https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling#Keypad
const dtmf = Object.freeze({
  1: [freqs.L_697, freqs.H_1209],
  2: [freqs.L_697, freqs.H_1336],
  3: [freqs.L_697, freqs.H_1477],
  A: [freqs.L_697, freqs.H_1633],

  4: [freqs.L_770, freqs.H_1209],
  5: [freqs.L_770, freqs.H_1336],
  6: [freqs.L_770, freqs.H_1477],
  B: [freqs.L_770, freqs.H_1633],

  7: [freqs.L_852, freqs.H_1209],
  8: [freqs.L_852, freqs.H_1336],
  9: [freqs.L_852, freqs.H_1477],
  C: [freqs.L_852, freqs.H_1633],

  '*': [freqs.L_941, freqs.H_1209],
  0: [freqs.L_941, freqs.H_1336],
  '#': [freqs.L_941, freqs.H_1477],
  D: [freqs.L_941, freqs.H_1633]
})

/*
 * tones: Array of objects. Each object has:
 * f1: first frequency
 * f2: second frequency
 * d: duration of sound phase
 * s: duration of silence phase
 * t: number of times the two phases will be repeated.
 */
const signals = {
  o1: undefined, // OscillatorNode 1
  o2: undefined, // OscillatorNode 2
  i: undefined, // intervalId from setInterval
  tones: Object.freeze({
    // @see https://en.wikipedia.org/wiki/Busy_signal#Styles
    busy: {
      f1: freqs.B_480,
      f2: freqs.B_620,
      d: 0.5,
      s: 0.5,
      t: 3
    },
    // @see https://en.wikipedia.org/wiki/Ringing_tone#North_America
    ringing_tone: {
      f1: freqs.BASE_440,
      f2: freqs.B_480,
      d: 2,
      s: 4,
      t: Infinity
    },
    // @see https://en.wikipedia.org/wiki/Reorder_tone
    recorder: {
      f1: freqs.B_480,
      f2: freqs.B_620,
      d: 0.25,
      s: 0.2,
      t: 5
    },
    // @see https://en.wikipedia.org/wiki/Disconnect_tone
    disconnect: {
      f1: freqs.BASE_425,
      f2: freqs.BASE_425,
      d: 0.25,
      s: 0.1,
      t: 3
    }
  })
}

const ContextClass = window.AudioContext ||
  window.webkitAudioContext ||
  window.mozAudioContext ||
  window.oAudioContext ||
  window.msAudioContext

/**
 * @type AudioContext
 */
let ctx

function error () {
  console.error(new Error('AudioContext is not supported by this browser!'))
}

/**
 * Plays simple oscillator sound, and stops automatically after a given time.
 *
 * @param {AudioContext} audioCtx
 * @param {number} frequency - OscillatorNode.frequency
 * @param {number} duration
 * @param {number} gain - GainNode.gain
 * @return {OscillatorNode}
 */
function getOscillator (audioCtx, frequency, duration, gain = 0.01) {
  // @see https://developer.mozilla.org/en-US/docs/Web/API/OscillatorNode
  const oscillatorNode = audioCtx.createOscillator()
  oscillatorNode.type = 'sine'
  oscillatorNode.frequency.value = frequency
  oscillatorNode.onended = () => {
    oscillatorNode.disconnect()
  }

  // @see https://developer.mozilla.org/en-US/docs/Web/API/GainNode
  const gainNode = audioCtx.createGain ? audioCtx.createGain() : audioCtx.createGainNode()
  gainNode.gain.value = gain

  // OscillatorNode => GainNode => Destination
  oscillatorNode.connect(gainNode, 0, 0)
  gainNode.connect(audioCtx.destination)

  oscillatorNode.start ? oscillatorNode.start(audioCtx.currentTime) : oscillatorNode.noteOn(0)
  oscillatorNode.stop(audioCtx.currentTime + duration)

  return oscillatorNode
}

/**
 * Plays predefined signal according to the key.
 *
 * @param {AudioContext} audioCtx
 * @param {string} key
 */
function playSignal (audioCtx, key) {
  if (!Object.keys(signals.tones).includes(key)) {
    return false
  }

  let x = 0

  const play = () => {
    signals.o1 = getOscillator(audioCtx, signals.tones[key].f1, signals.tones[key].d)
    signals.o2 = getOscillator(audioCtx, signals.tones[key].f2, signals.tones[key].d)

    x++

    if (x >= signals.tones[key].t && signals.i) {
      clearInterval(signals.i)
    }
  }

  play()

  if (signals.tones[key].t > 1) {
    signals.i = setInterval(play, (signals.tones[key].d + signals.tones[key].s) * 1000)
  }
}

/**
 * Plays one of the predefined signals.
 *
 * @param {string} key
 */
function play (key) {
  stop()

  playSignal(ctx, key)
}

/**
 * Stops playing signals. Doesn't affect DTMF signals.
 */
function stop () {
  if (signals.i) {
    clearInterval(signals.i)
  }
  if (typeof signals.o1 !== 'undefined') {
    signals.o1.disconnect()
  }
  if (typeof signals.o2 !== 'undefined') {
    signals.o2.disconnect()
  }
}

/**
 *
 * @param {string}  key
 */
function playDTMF (key) {
  if (Object.keys(dtmf).includes(key)) {
    getOscillator(ctx, dtmf[key][0], 0.2)
    getOscillator(ctx, dtmf[key][1], 0.2)
  }
}

/**
 * Returns the functions to play/stop signal sounds.
 *
 * @param {AudioContext|null} audioCtx Optional AudioContext
 * @return {{play: function, stop: function, playDTMF: function}}
 */
export default function teleSignalsInit (audioCtx = null) {
  // If AudioContext is not supported, then abort!
  if (!ContextClass) {
    error()

    return {
      play: error,
      playDTMF: error,
      stop: error
    }
  }

  // Always prioritize the passed AudioContext, otherwise use the exciting one or create it.
  if (audioCtx) {
    ctx = audioCtx
  } else if (!ctx) {
    ctx = new ContextClass()
  }

  return {
    play,
    playDTMF,
    stop
  }
}
