# TeleSignals [Demo](https://phonelift.gitlab.io/telesignals)
A light weight library that plays standard famous telephone signals without binary sound files, using Web Audio API.

## Installation
```bash
npm i @phonelift/telesignals
```

## Usage
```javascript
import teleSignalsInit from '@phonelift/telesignals'
// Or find it in window.teleSignalsInit if you use the UMD format file.

const teleSignals = teleSignalsInit()

// Attach listeners to all buttons. When a button is clicked, read its content and pass it to playDTMF()
// Check the file /static/index.html for the HTML
document.querySelectorAll('#keypad button')
  .forEach(button => {
    button.addEventListener('click', function (event) {
      teleSignals.playDTMF(event.target.innerHTML)
    })
  })

window.addEventListener('keydown', function (event) {
  teleSignals.playDTMF(event.key)
})

// More examples in /static/dev.js
```
#### teleSignalsInit(audioCtx = null)
The only function that is exported by the module.

It tales one argument of the type `AudioContext`. If you don't provide any, it will create its own internal instance of `AudioContext`.

From the [Web Audio API specification proposal](https://www.w3.org/2011/audio/drafts/1WD/WebAudio/):
> In most use cases, only a single AudioContext is used per document.

So, if your app already has one, then it's a good idea to pass it to this function.

teleSignalsInit returns an object that has three functions:

| Function |
| ------ |
| playDTMF(key: string) |
| play(key: string) |
| stop() |

##### playDTMF(key: string)
Plays a [DTMF](https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling) tone.
`key` can possibly be one of the following values: `0-9`, `A-D`, `#` and `*`. Any other values will be ignored.

##### play(key: string)
Plays one of the pre-defined signals.
`key` can possibly be one of the following values:
- `busy` to play [Busy signal](https://en.wikipedia.org/wiki/Busy_signal)
- `ringing_tone` to play [Ringing tone](https://en.wikipedia.org/wiki/Ringing_tone#North_America)
- `recorder` to play [Reorder tone](https://en.wikipedia.org/wiki/Reorder_tone)
- `disconnect` to play [Disconnect tone](https://en.wikipedia.org/wiki/Disconnect_tone)

Calling one of these while the other is playing will stop the previous one and initiate the new one.

#### stop()
Stops playing one of the predefined signals that are started by `play(key: string)`.
This doesn't affect DTMF tones initiated by `playDTMF(key: string)`.

## Browser Compatibility
This library makes use of the [Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API), therefore it has the same browser support.

## Issues and Bugs
Please submit your requests or report issues and bugs [here](https://gitlab.com/phonelift/telesignals/issues).

## Contribution
Feel free to discuss any addition or improvement before you submit a merge request.
Please don't commit anything in the dist folder. This folder gets updated just before a release.

## License
MIT
