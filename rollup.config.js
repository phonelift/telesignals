import babel from 'rollup-plugin-babel'
import { uglify } from 'rollup-plugin-uglify'
import pkg from './package.json'

export default [
  {
    input: 'src/index.js',
    plugins: [
      babel()
    ],
    output: [
      {
        format: 'es',
        file: pkg.module
      }
    ]
  },
  {
    input: 'src/index.js',
    plugins: [
      babel(),
      uglify()
    ],
    output: [
      {
        format: 'umd',
        file: pkg.browser,
        sourcemap: true,
        name: 'teleSignalsInit'
      }
    ]
  }
]
